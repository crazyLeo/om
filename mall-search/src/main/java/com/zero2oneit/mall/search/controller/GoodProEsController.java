package com.zero2oneit.mall.search.controller;

import com.zero2oneit.mall.common.utils.EsIndexConstant;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.search.qo.IndexSearchQo;
import com.zero2oneit.mall.search.qo.ShopTypeGoodQo;
import com.zero2oneit.mall.search.service.GoodProEsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @date 2021/3/17 -15:23
 */
@RestController
@RequestMapping("/searchProduct")
public class GoodProEsController {

    @Autowired
    private GoodProEsService goodProEsService;

    /**
     * 首页商品列表
     * @param qo
     * @return
     */
    @PostMapping("/indexProList")
    public R indexProList(@RequestBody IndexSearchQo qo){
        return R.ok(goodProEsService.indexProList(qo));
    }

    /**
     * 查询商品详情
     * @param
     * @return
     */
    @PostMapping("/productDetail/{id}")
    public R productDetail(@PathVariable String id){
        Map goodMap = goodProEsService.getByIndexAndId(EsIndexConstant.GOOD_INDEX,id);
        List list = goodProEsService.productSkuByProId(id);
        Map<String,Object> map=new HashMap<>(2);
        map.put("good",goodMap);
        map.put("skuList",list);
        return R.ok(map);
    }

    /**
     * 查询社区商品详情
     * @param
     * @return
     */
    @PostMapping("/yzProductDetail/{id}")
    public R yzProductDetail(@PathVariable String id){
        Map goodMap = goodProEsService.getByIndexAndId(EsIndexConstant.COMMUNITY_GOOD_INDEX,id);
        List list = goodProEsService.productSkuByProId(id);
        Map<String,Object> map=new HashMap<>(2);
        map.put("good",goodMap);
        map.put("skuList",list);
        return R.ok(map);
    }

    /**
     * 根据skuid查询sku信息 已购商品页面
     * @param
     * @return
     */
    @PostMapping("/purchasedSku")
    public R purchasedSku(@RequestBody List<Map> list){
        return R.ok(goodProEsService.purchasedSku(list));
    }

    /**
     * 根据skuid查询sku信息 包邮
     * @param
     * @return
     */
    @PostMapping("/scProductSku/{id}/{productId}")
    public R scProductSku(@PathVariable String id,@PathVariable String productId){
        return R.ok(goodProEsService.yzProductSku(EsIndexConstant.GOOD_SKU_INDEX,EsIndexConstant.GOOD_INDEX,id,productId));
    }

    /**
     * 根据skuid查询sku信息 社区
     * @param
     * @return
     */
    @PostMapping("/yzProductSku/{id}/{productId}")
    public R yzProductSku(@PathVariable String id,@PathVariable String productId){
        return R.ok(goodProEsService.yzProductSku(EsIndexConstant.COMMUNITY_SKU_INDEX,EsIndexConstant.COMMUNITY_GOOD_INDEX,id,productId));
    }

    /**
     * 根据skuIdList查询库存
     * @param
     * @return
     */
    @PostMapping("/skuListStock")
    public R skuListStock(@RequestParam(value = "list") List<String> list){
        return R.ok(goodProEsService.skuListStock(list));
    }

    /**
     * 分类查询
     * @param map id 分类id ，size 每页显示多少条记录 ，currentPage 当前页 ，level 分类级别,sort ASC是升序 DESC 是降序 sales 是销量
     * @return
     */
    @PostMapping("/sortSearch")
    public R sortSearch(@RequestBody Map<String,String> map){
        return R.ok(goodProEsService.sortSearch(map));
    }

    /**
     * 全文检索，检索商品名称、商品详情、商品分类
     * @param map keyword 检索关键词 ，size 每页显示多少条记录 ，currentPage 当前页 ，sort ASC是升序 DESC 是降序
     * @return
     */
    @PostMapping("/keywordSearch")
    public R keywordSearch(@RequestBody Map<String,String> map){
        return goodProEsService.keywordSearch(map);
    }

    /**
     * 商铺内检索商品
     * @param map businessId 商铺id  keyword 商品关键词
     * @return
     */
    @PostMapping("/myProSearch")
    public R myProSearch(@RequestBody Map<String,String> map){
        return R.ok(goodProEsService.myProSearch(map,EsIndexConstant.GOOD_INDEX));
    }

    /**
     * 社区内检索商品
     * @param map   keyword 商品关键词
     * @return
     */
    @PostMapping("/yzProSearch")
    public R yzProSearch(@RequestBody Map<String,String> map){

        return R.ok(goodProEsService.yzProSearch(map));
    }

    /**
     * 商铺检索
     * @param map  keyword 商店关键词
     * @return
     */
    @PostMapping("/businessSearch")
    public R businessSearch(@RequestBody Map<String,String> map){
        return R.ok(goodProEsService.businessSearch(map));
    }


    /**
     * 社区商品，社区首页，分页，点击分类选
     * @param
     * @return
     */
    @PostMapping("/yzBusinessTypeSearch")
    public R yzBusinessTypeSearch(@RequestBody ShopTypeGoodQo qo){
        return goodProEsService.yzBusinessTypeSearch(qo);
    }

    /**
     * 点击店铺分类
     * @param
     * @return String businessId 商家id  String bussTypeId商家分类id
     */
    @PostMapping("/businessType")
    public R businessType(@RequestBody ShopTypeGoodQo qo){

        return R.ok(goodProEsService.businessType(qo));
    }

    /**
     * 爆款，推介
     * @param
     * @return
     */
    @PostMapping("/hotGood")
    public R hotGood(){
        return R.ok(goodProEsService.hotGood());
    }
}
