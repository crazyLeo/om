package com.zero2oneit.mall.market.controller;

import com.zero2oneit.mall.common.bean.market.Advert;
import com.zero2oneit.mall.common.query.market.AdvertQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.market.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-01-25
 */
@RestController
@RequestMapping("/admin/advert")
public class AdvertController {

    @Autowired
    private AdvertService advertService;

    /**
     * 查询广告列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody AdvertQueryObject qo){
        return advertService.pageList(qo);
    }

    /**
     * 添加或编辑广告信息
     * @param advert
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody Advert advert){
        advertService.saveOrUpdate(advert);
        return R.ok();
    }

    /**
     * 根据ID删除对应的广告信息
     * @param ids
     * @return
     */
    @PostMapping("/deleteByIds")
    public R deleteByIds(@RequestBody String ids){
        return advertService.removeByIds(Arrays.asList(ids.split(","))) == true ? R.ok("删除成功") : R.fail("删除失败");
    }

}
