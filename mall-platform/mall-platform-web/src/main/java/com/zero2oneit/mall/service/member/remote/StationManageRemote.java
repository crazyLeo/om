package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.annotion.OperateLog;
import com.zero2oneit.mall.common.bean.goods.GoodSaleArea;
import com.zero2oneit.mall.common.bean.member.StationManage;
import com.zero2oneit.mall.common.enums.BusinessType;
import com.zero2oneit.mall.common.query.goods.GoodsCategoryQueryObject;
import com.zero2oneit.mall.common.query.member.StationManageQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.StationManageFeign;
import com.zero2oneit.mall.feign.oss.OssFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: 远程调用驿站管理服务
 *
 * @author yjj
 * @date 2021/3/1 10:51
 */
@RestController
@RequestMapping("/remote/stationManage")
public class StationManageRemote {

    @Autowired
    private StationManageFeign stationManageFeign;

    @Autowired
    private OssFeign ossFeign;

    /**
     * 根据站点名称、站长名称、站点电话获取对应的列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody StationManageQueryObject qo) {
        return stationManageFeign.list(qo);
    }

    /**
     * 添加或编辑驿站信息
     * @param stationManage
     * @param file
     * @return
     */
    @OperateLog(title = " 添加或编辑驿站信息", businessType = BusinessType.UPDATE)
    @PostMapping("/addOrEdit")
    public R addOrEdit(StationManage stationManage, @RequestParam("file") MultipartFile[] file){
        String imgUrl = null;
        if (file != null && file.length >= 1) {
            imgUrl = ossFeign.uploadImage(file, "stationManage");
            System.out.println(imgUrl);
            if(stationManage.getId() == null){
                stationManage.setPicture(imgUrl);
            }else {
                stationManage.setPicture(imgUrl + (stationManage.getPicture().length() > 0 ? ","+stationManage.getPicture() : ""));
            }
        }
        return stationManageFeign.addOrEdit(stationManage);
    }

    /**
     * 获取销售区域(社区)树集合
     * @return
     */
    @PostMapping("/tree")
    public R tree() {
        return R.ok(stationManageFeign.tree());
    }

    /**
     * 驿站状态开关：编辑
     * @param value
     * @param id
     * @return
     */
    @PostMapping("/stationStatusEdit")
    public R stationStatusEdit(String value, String id){
        return stationManageFeign.stationStatusEdit(value,id);
    }

    /**
     * 驿站审核申请
     * @param ids
     * @return
     */
    @PostMapping("/checkByIds")
    public R checkByIds(String ids){
        return stationManageFeign.checkByIds(ids);
    }

    /**
     * 根据ID删除驿站信息
     * @param ids
     * @return
     */
    @PostMapping("/delByIds")
    public R delByIds(String ids){
        return stationManageFeign.delByIds(ids);
    }
}
